# Docker Environment for NodeJS

## Installed items
- Ubuntu 12.04
- NodeJS
- Git

```
- Host machine contains the project files and are shared with docker container
- Bash-it shell installed for nice git interface
```
**Build and run instructions are in the Dockerfile.**
