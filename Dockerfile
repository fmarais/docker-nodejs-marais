FROM node:latest

# --------------------- Build and Run
# 	 	docker build -t nodejs/marais .

#  ---------- without redis
#  1st time (no container)
#		sudo docker run -it \
#		--net host \
#		-v $HOME/projects/nodejs:/home/developer/projects \
#		--name nodejs \
#		nodejs/marais \
#		node /home/developer/projects/ems-response-track-chat-server/index.js

#  2nd time (container exists)
# 	 	sudo docker start nodejs
#  ---------- without redis

#  ---------- with redis
#  1st time (no container)
#  		1. start redis
#		2. start nodejs linked to redis
#
#		docker run -d -p 6379:6379 --name redis redis && \
#		docker run -it \
#		-p 3000:3000 \
#		-v $HOME/projects/nodejs:/home/developer/projects \
#		--name nodejs \
#		--link redis:redis nodejs/marais \
#		node /home/developer/projects/ems-response-track-chat-server/index.js

#  2nd time (container exists)
# 	 	docker start redis && docker start -i nodejs
#  ---------- with redis

# Expose the port
EXPOSE 3000

# --------------------- Create home dir
RUN mkdir -p /home/developer

# --------------------- Init and tools
RUN apt-get update
RUN apt-get install -y \
nano \
unzip \
git

# Bash-it for git shell
RUN git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it && \
~/.bash_it/install.sh